//
//  FYPMainMenuController.h
//  charity_donation_system
//
//  Created by Jason on 1/7/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#define biddingActionSheetIndex 0
#define directBuyActionSheetIndex 1
#define thankActionSheetIndex 2

#import <UIKit/UIKit.h>

@interface FYPMainMenuController : UIViewController <UIActionSheetDelegate>
- (IBAction)postItemButtonClicked:(UIButton *)sender;

@end
