//
//  Member.m
//  charity_donation_system
//
//  Created by Jason on 12/29/12.
//  Copyright (c) 2012 ive. All rights reserved.
//

#import "Member.h"

@interface Member()

@end

@implementation Member

static Member *member = nil;


/*init the member with the data downloaded back from the web server*/
+(Member *) initWithData:(NSData *)data{
   
    Member *newMember = [[Member alloc]init];
    
    /*parse the data download from the controller
     example of returned JSON String*
     {"allPhotos":"","created_at":"2012-12-08T05:39:58Z","email":"","firstName":"","id":1,"lastName":"","mobileNo":12,"password":"1234","updated_at":"2012-12-11T08:55:07Z","username":"jason"}
     
     */
    
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    newMember.username = jsonObject[@"username"];
    newMember.password = jsonObject[@"password"];
    newMember.id = [jsonObject[@"id"] intValue];

    
    return newMember;
}

+(Member *) createMember:(NSData *)data{
    if (member == nil) {
        Member *newMember = [self initWithData:data];
        member = newMember;
    }
    
    
    return member;
}

+(Member *)member{
    return member;
}
@end



