//
//  FYPPostDirectItemViewController.h
//  charity_donation_system
//
//  Created by Jason on 4/6/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSAssetPicker.h"
#import "AsyncConnection.h"
#import "FYPSearchCategoryPickerViewController.h"
#import "FYPLongTextViewController.h"

#define sevenIndex 0
#define fourtheenIndex 1
#define twentyOneIndex 2


@interface FYPPostDirectItemViewController : UITableViewController <UINavigationControllerDelegate, WSAssetPickerControllerDelegate, AsyncConnectionDelegate, FYPSearchCategoryPickerViewDelegate, FYPSearchCharityViewDelegate, FYPLongTextViewControllerDelegate>

#pragma mark - Open property for other controllers
@property (nonatomic, weak) UIViewController *mainMenuController;

#pragma mark - UI
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *charityOrganizationLabel;

@property (weak, nonatomic) IBOutlet UILabel *itemNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemDescriptionLabel;
@property (weak, nonatomic) IBOutlet UITextField *priceTextField;
@property (weak, nonatomic) IBOutlet UITextField *donationProportionTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *durationSegmentedControl;


- (IBAction)getPhotoButtonClicked:(UIBarButtonItem *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
