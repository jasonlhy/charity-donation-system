//
//  FYPPostDirectItemViewController.m
//  charity_donation_system
//
//  Created by Jason on 4/6/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import "FYPPostDirectItemViewController.h"
#import "NSData+Base64.h"
#import "Category.h"
#import "CharityOrganization.h"
#import "Member.h"

#define categoryIndex 0
#define charityOrganiztionIndex 1

@interface FYPPostDirectItemViewController ()

@property (nonatomic, strong) WSAssetPickerController *picker;
@property (nonatomic, strong) NSArray *imageArray;

@property (strong, nonatomic) Category* selectedCategory;
@property (strong, nonatomic) CharityOrganization *selectedCharityOrganization;

@property (strong, nonatomic) UIViewController *presentingItemNameTextController;
@property (strong, nonatomic) UIViewController *presentingItemDescriptionTextController;

@property (strong, nonatomic) NSDictionary *infoJson;

@end

@implementation FYPPostDirectItemViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)getPhotoButtonClicked:(UIBarButtonItem *)sender {
    WSAssetPickerController *controller = [[WSAssetPickerController alloc] initWithDelegate:self];
    [self presentViewController:controller animated:YES completion:NULL];
}

#pragma mark - WSAssestPickerController Delegate Method
- (void)assetPickerControllerDidCancel:(WSAssetPickerController *)sender
{
    // Dismiss the WSAssetPickerController.
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)assetPickerController:(WSAssetPickerController *)sender didFinishPickingMediaWithAssets:(NSArray *)assets
{
    // Hang on to the picker to avoid ALAssetsLibrary from being released (see note below).
    self.picker = sender;
    
    // Dismiss the WSAssetPickerController.
    __block id weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        
        // Do something with the assets here.
        
        
        
        // Release the picker.
        [weakSelf setPicker:nil];

    }];
    
    
    
    // convert the ALAssets array into UIImage array
    NSMutableArray *imageArray = [NSMutableArray new];
    for (int i =0; i<[assets count]; i++) {
        CGImageRef imageRef = [[assets objectAtIndex:i] thumbnail];
        [imageArray addObject:[UIImage imageWithCGImage:imageRef]];
    }
    self.imageArray = imageArray;
    
    self.imageView.animationImages      = imageArray;
    self.imageView.animationDuration    = 2*[imageArray count];
    self.imageView.animationRepeatCount = 0;
    
    [self.imageView startAnimating];
    
    //    AsyncConnection *connection = [[AsyncConnection alloc] init];
    //
    //
    //    [connection callAsyncConnectionAtUrl:@"http://localhost:3000/items/create_with_photos" dictionary:[self convertoViewDataToJSON] method:@"POST" delegate:self];
    
    self.infoJson = [self convertoViewDataToJSON];
    [self performSegueWithIdentifier:@"getPhoto" sender:self];
}

- (void)finishConnectionWithData:(NSData *)data connection:(AsyncConnection *)connection{
    
}

#pragma mark - go to next screen
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"searchCategory"] || [segue.identifier isEqualToString:@"searchCharityOrganization" ] || [segue.identifier isEqualToString:@"getItemName"] || [segue.identifier isEqualToString:@"getItemDescription"]) {
        UIViewController *destinationController = segue.destinationViewController;
        if ([destinationController respondsToSelector:@selector(delegate)]) {
            [destinationController setValue:self forKey:@"delegate"];
        }
    }
    
    if ( [segue.identifier isEqualToString:@"getItemName"]) {
        UIViewController *destinationController = segue.destinationViewController;
        self.presentingItemNameTextController = segue.destinationViewController;
        if ([destinationController respondsToSelector:@selector(titleOfText)]) {
            [destinationController setValue:@"Item Name" forKey:@"titleOfText"];
        }
        if ([destinationController respondsToSelector:@selector(longText)]) {
            [destinationController setValue:self.itemNameLabel.text forKey:@"longText"];
        }
    }
    
    if ( [segue.identifier isEqualToString:@"getItemDescription"]) {
        UIViewController *destinationController = segue.destinationViewController;
        self.presentingItemDescriptionTextController = segue.destinationViewController;
        if ([destinationController respondsToSelector:@selector(titleOfText)]) {
            [destinationController setValue:@"Item Description" forKey:@"titleOfText"];
        }
        if ([destinationController respondsToSelector:@selector(longText)]) {
            [destinationController setValue:self.itemDescriptionLabel.text forKey:@"longText"];
        }
    }
    
    if ([segue.identifier isEqualToString:@"getPhoto"]) {
        UIViewController *destinationController = segue.destinationViewController;
        self.presentingItemDescriptionTextController = segue.destinationViewController;
        if ([destinationController respondsToSelector:@selector(infoJson)]) {
            [destinationController setValue:self.infoJson forKey:@"infoJson"];
        }
        if ([destinationController respondsToSelector:@selector(imageArray)]) {
            [destinationController setValue:self.imageArray forKey:@"imageArray"];
        }
        
        if ([destinationController respondsToSelector:@selector(mainMenuController)]) {
            [destinationController setValue:self.mainMenuController forKey:@"mainMenuController"];
        }
    }
}
# pragma mark - TableViewDelegate
-(NSIndexPath* )tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    if (indexPath.section == 0 && indexPath.row == categoryIndex) {
//        [self performSegueWithIdentifier:@"searchCategory" sender:self];
//        return indexPath;
//    }
//    if (indexPath.section == 0 && indexPath.row == charityOrganiztionIndex) {
//        [self performSegueWithIdentifier:@"searchCharityOrganization" sender:self];
//        return indexPath;
//    }
    
    if ([self requireAnotherViewControllerForInput:indexPath]) {
        return indexPath;
    }

    
    
    return nil;
}

#pragma mark - Delegate method of SearchPickerDelegate
- (void)didFininishPickAnCategory:(Category *)category{
    [self dismissViewControllerAnimated:YES completion:nil];
    self.selectedCategory = category;
    self.categoryLabel.text = category.name;
}

- (void)didUnSelectCategory{
    [self dismissViewControllerAnimated:YES completion:nil];
    self.selectedCategory = nil;
    self.categoryLabel.text = nil;
}

#pragma mark - Delegate methods of FYPSearchCharityViewDelegate
- (void)didFininishPickAnCharityOrganizations:(CharityOrganization *)charityOrganization{
    self.selectedCharityOrganization = charityOrganization;
    self.charityOrganizationLabel.text = charityOrganization.name;
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didUnSelectCharityOrganization{
    self.selectedCharityOrganization = nil;
    self.charityOrganizationLabel.text = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)didFinishEditingOnExit:(FYPLongTextViewController *)controller contentInTextView:(NSString *)content{
    // distingush the controller purpose
    if (self.presentingItemNameTextController == controller) {
        self.itemNameLabel.text = content;
        self.presentingItemNameTextController = nil;
    }
    else if (self.presentingItemDescriptionTextController == controller){
        self.itemDescriptionLabel.text = content;
        self.presentingItemDescriptionTextController = nil;
    }
}

# pragma mark - customerized method
-(BOOL)requireAnotherViewControllerForInput:(NSIndexPath *)indexPath{
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    
    
    if (section == 0 || section == 1) {
        if (row == 0 || row == 1) {
            return YES;
        }
    }
    
    return NO;
}

-(NSInteger)getEndDate{
    NSInteger end_date = 7;
    NSInteger index = self.durationSegmentedControl.selectedSegmentIndex;
    if (index == fourtheenIndex) {
        end_date = 14;
    }
    else if (index == twentyOneIndex){
        end_date = 21;
    }
    
    return end_date;
}

-(NSDictionary *)convertoViewDataToJSON{
    // base information
    NSMutableDictionary *keyValue =[[NSMutableDictionary alloc]init];
//    [keyValue setObject:@"testing with seller" forKey:@"name"];
//    [keyValue setObject:@"description" forKey:@"description"];
//    [keyValue setObject:@(12) forKey:@"donation_proportion"];
//    [keyValue setObject:@(1222) forKey:@"price"];
//    [keyValue setObject:@"BiddingItem" forKey:@"type"];
//    [keyValue setObject:@(7) forKey:@"end_date"];
//    [keyValue setObject:@"fewfew" forKey:@"remote_itemPhotos_attribute"];
//    [keyValue setObject:@(2) forKey:@"category_id"];
//    [keyValue setObject:@(1) forKey:@"seller_id"];
    
    NSInteger categoryID = self.selectedCategory.id;
    NSInteger charityOrganizationID = self.selectedCharityOrganization.id;
    Member *member = [Member member];    
    NSInteger end_date = [self getEndDate];
    
    [keyValue setObject:self.itemNameLabel.text forKey:@"name"];
    [keyValue setObject:self.itemDescriptionLabel.text forKey:@"description"];
    [keyValue setObject:@([self.donationProportionTextField.text doubleValue]) forKey:@"donation_proportion"];
    [keyValue setObject:@([self.priceTextField.text doubleValue]) forKey:@"price"];
    [keyValue setObject:@"BiddingItem" forKey:@"type"];
    [keyValue setObject:@(end_date) forKey:@"end_date"];
    [keyValue setObject:@(categoryID) forKey:@"category_id"];
    [keyValue setObject:@(charityOrganizationID) forKey:@"charity_organization_id"];
    [keyValue setObject:@(member.id) forKey:@"seller_id"];
    
    
    
    // photos
//    NSMutableArray *encodedPhotoArray = [NSMutableArray new];
//    for (int i =0; i<[self.imageArray count]; i++) {
//        NSMutableDictionary *itemPhoto= [NSMutableDictionary new ];
//        NSData *imageData = UIImagePNGRepresentation([self.imageArray objectAtIndex:i]);
//        NSString *imageDataEncodedString = [imageData base64EncodedString];
//        [itemPhoto setObject:@"description" forKey:@"description"];
//        [itemPhoto setObject:imageDataEncodedString forKey:@"file"];
//        
//        [encodedPhotoArray addObject:itemPhoto];
//    }
//    [keyValue setObject:encodedPhotoArray forKey:@"remote_itemPhotos_attribute"];
    
    return keyValue;
    


    
}
@end
    

