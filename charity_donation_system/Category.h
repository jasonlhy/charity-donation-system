//
//  Category.h
//  charity_donation_system
//
//  Created by Jason on 1/19/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import <Foundation/Foundation.h>

/*An model class of Category*/
@interface Category : NSObject

// primitive data type
@property (nonatomic) NSUInteger id;
@property (nonatomic) NSUInteger previousCategoryId;
@property (nonatomic) NSUInteger level;

// pointer data type
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSArray* subCategories;

// init a Category
-(id)initWithData:(NSData *)data;
-(id)initWithDictionary:(NSDictionary *)dictionary;

+(Category *)findCategoeryWithId:(NSUInteger)id;


// create a array with categories from JSONArray
+(NSArray *)categoriesWithArray:(NSArray *)jsonArray;

// singleton array of holding categories
+(NSArray *)categories;
+(NSDictionary *)categoriesDictionary;
@end
