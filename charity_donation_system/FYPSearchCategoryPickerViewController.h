//
//  FYPSearchPickerViewController.h
//  charity_donation_system
//
//  Created by Jason on 1/24/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FYPSearchViewController.h"
#import "Category.h"

@class FYPSearchViewController;
@class Category;
@protocol FYPSearchCategoryPickerViewDelegate;


#define kParentCategory   0
#define kSubCategory     1

@interface FYPSearchCategoryPickerViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btn;
@property (weak, nonatomic) id <FYPSearchCategoryPickerViewDelegate> delegate;

- (IBAction)selectedButtonClicked:(UIBarButtonItem *)sender;
- (IBAction)clearButtonClicked:(UIBarButtonItem *)sender;


@end

@protocol FYPSearchCategoryPickerViewDelegate <NSObject>
- (void)didFininishPickAnCategory:(Category *)category;
- (void)didUnSelectCategory;
@end