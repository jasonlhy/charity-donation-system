//
//  FYPSearchPickerViewController.m
//  charity_donation_system
//
//  Created by Jason on 1/24/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import "FYPSearchCategoryPickerViewController.h"

@interface FYPSearchCategoryPickerViewController ()

@property (strong, nonatomic) NSDictionary *categorieDictionary;
@property (strong, nonatomic) NSArray *parentCategoryKey;
@property (strong, nonatomic) NSArray *subCategories;

@end

@implementation FYPSearchCategoryPickerViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.categorieDictionary = [Category categoriesDictionary];
    self.parentCategoryKey  = [self.categorieDictionary allKeys];
    
    NSLog(@"parentCategoyrKey %@",self.parentCategoryKey );
    
    NSString *selectedCategory = [self.parentCategoryKey objectAtIndex:0];
    NSArray *array = [self.categorieDictionary objectForKey:selectedCategory];
    self.subCategories = array;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Picker Data Source Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component {
    if (component == kParentCategory){
        return [self.parentCategoryKey count];
    }
    else{
        return [self.subCategories count];
    }
}

#pragma mark Picker Delegate Methods
- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component {
    if (component == kParentCategory){
        id object = [self.parentCategoryKey objectAtIndex:row];
        return object;
    }
    else{
        Category *catetogry = [self.subCategories objectAtIndex:row];
        return catetogry.name;
    }
}

- (void)pickerView:(UIPickerView *)pickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    if (component == kParentCategory) {
        NSString *selectedState = [self.parentCategoryKey objectAtIndex:row];
        NSArray *array = [self.categorieDictionary objectForKey:selectedState];
        self.subCategories = array;
        [self.picker selectRow:0 inComponent:kSubCategory animated:YES];
        [self.picker reloadComponent:kSubCategory];
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView
    widthForComponent:(NSInteger)component {
    if (component == kSubCategory)
        return 90;
    return 200;
}




- (IBAction)selectedButtonClicked:(UIBarButtonItem *)sender {
    
    NSUInteger subCategoryIndex = [self.picker selectedRowInComponent:1];
    Category *selectedCategory = [self.subCategories objectAtIndex:subCategoryIndex];
    [self.delegate didFininishPickAnCategory:selectedCategory];
}

- (IBAction)clearButtonClicked:(UIBarButtonItem *)sender {
    
    [self.delegate didUnSelectCategory];
}
@end
