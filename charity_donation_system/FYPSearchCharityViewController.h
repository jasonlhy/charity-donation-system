//
//  FYPSearchCharityViewController.h
//  charity_donation_system
//
//  Created by Jason on 1/24/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CharityOrganization.h"

@protocol FYPSearchCharityViewDelegate;

@interface FYPSearchCharityViewController : UIViewController <UIPickerViewDelegate,UIPickerViewDataSource>

@property (strong, nonatomic) IBOutlet UIPickerView *singlePicker;
@property (strong, nonatomic) NSArray *charityOrganizations;

@property (weak, nonatomic) id <FYPSearchCharityViewDelegate> delegate;

- (IBAction)selectButtonClicked:(UIBarButtonItem *)sender;
- (IBAction)clearButtonClicked:(UIBarButtonItem *)sender;


@end

@protocol FYPSearchCharityViewDelegate <NSObject>
- (void)didFininishPickAnCharityOrganizations:(CharityOrganization *)charityOrganization;
- (void)didUnSelectCharityOrganization;
@end