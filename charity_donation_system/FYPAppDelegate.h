//
//  FYPAppDelegate.h
//  charity_donation_system
//
//  Created by Jason on 12/29/12.
//  Copyright (c) 2012 ive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
