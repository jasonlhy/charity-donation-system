//
//  FYPSearchViewController.h
//  charity_donation_system
//
//  Created by Jason on 1/20/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#define searchCategories 1
#define searchOrganization 2

#import <UIKit/UIKit.h>
#import "FYPSearchCategoryPickerViewController.h"
#import "FYPSearchCharityViewController.h"

@protocol FYPSearchCategoryPickerViewDelegate;
@protocol FYPSearchCharityViewDelegate;

@interface FYPSearchViewController : UITableViewController <FYPSearchCategoryPickerViewDelegate, FYPSearchCharityViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *titleInput;
@property (weak, nonatomic) IBOutlet UILabel *categoryNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *charityOrganizationLabel;

- (IBAction)searchBtnClicked:(id)sender;

@end
