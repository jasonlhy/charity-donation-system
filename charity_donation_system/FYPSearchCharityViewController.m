//
//  FYPSearchCharityViewController.m
//  charity_donation_system
//
//  Created by Jason on 1/24/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import "FYPSearchCharityViewController.h"

@interface FYPSearchCharityViewController ()

@end

@implementation FYPSearchCharityViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Do any additional setup after loading the view from its nib.
    NSArray *array = [CharityOrganization charityOrganizations];
    self.charityOrganizations = array;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Picker Data Source Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component {
    return [self.charityOrganizations count];
}

#pragma mark Picker Delegate Methods
- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component {
    
    CharityOrganization *c =[self.charityOrganizations objectAtIndex:row];
    return c.name;
}

- (IBAction)selectButtonClicked:(UIBarButtonItem *)sender {
    NSUInteger selectedRow = [self.singlePicker selectedRowInComponent:0];
    CharityOrganization *selectedCharityOrganization = [self.charityOrganizations objectAtIndex:selectedRow];
    
    [self.delegate didFininishPickAnCharityOrganizations:selectedCharityOrganization];
}

- (IBAction)clearButtonClicked:(UIBarButtonItem *)sender {
    [self.delegate didUnSelectCharityOrganization];
}
@end
