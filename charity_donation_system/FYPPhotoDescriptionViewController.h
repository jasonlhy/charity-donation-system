//
//  FYPPhotoDescriptionViewController.h
//  charity_donation_system
//
//  Created by Jason on 4/7/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncConnection.h"

#define imageViewTag 100
#define textViewTag 200

@interface FYPPhotoDescriptionViewController : UICollectionViewController <AsyncConnectionDelegate, UITextViewDelegate>

@property (nonatomic) NSMutableDictionary *infoJson;
@property (nonatomic) NSArray *imageArray;
@property (nonatomic, weak) UIViewController *mainMenuController;

- (IBAction)postItem:(UIBarButtonItem *)sender;

@end
