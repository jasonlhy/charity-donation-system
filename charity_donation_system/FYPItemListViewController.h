//
//  FYPItemListViewController.h
//  charity_donation_system
//
//  Created by Jason on 1/20/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYPItemListViewController : UITableViewController

@property (weak, nonatomic) NSNumber *categoryId;

@property (strong, nonatomic) NSArray* items;

@end
