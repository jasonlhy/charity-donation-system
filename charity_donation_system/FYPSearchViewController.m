//
//  FYPSearchViewController.m
//  charity_donation_system
//
//  Created by Jason on 1/20/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import "FYPSearchViewController.h"
#import "AsyncConnection.h"
#import "Item.h"
#import "Category.h"
#import "CharityOrganization.h"

@interface FYPSearchViewController ()

@property (strong, nonatomic) NSArray* items;
@property (strong, nonatomic) Category* selectedCategory;
@property (strong, nonatomic) CharityOrganization *selectedCharityOrganization;

@end

@implementation FYPSearchViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)searchBtnClicked:(id)sender {
    
    // ip addreess
    NSDictionary* info = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Web Server Info"];
    NSString *ip = [info objectForKey:@"Domain ip"] ;
    NSString *searchPath = [info objectForKey:@"Search path"];
    
    NSString *url = [ip stringByAppendingString:searchPath];
    
    // data
    NSString *name = self.titleInput.text;
    NSString *category_id = (self.selectedCategory !=nil)?[NSString stringWithFormat:@"%d",self.selectedCategory.id]:@"";
    NSString *charity_organization_id = (self.selectedCharityOrganization !=nil)?[NSString stringWithFormat:@"%d",self.selectedCharityOrganization.id]:@"";
    
    url = [[NSString alloc] initWithFormat:@"%@?name=%@&category_id=%@&charity_organization_id=%@",url, name,category_id, charity_organization_id];
    NSLog(@"url: %@",url);
    
    // Connection
    AsyncConnection *connectiono = [[AsyncConnection alloc] init];
    
    [connectiono callAsyncConnectionAtUrl:url dictionary:nil method:@"GET" delegate:self ];
    
}

#pragma mark - Table Deleget Methods
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // return nil to prevent the table row being on focus
    // when the user tap the label
    NSUInteger rowIndex = indexPath.row;
    
    if (rowIndex == searchCategories ) {
        [self performSegueWithIdentifier:@"searchCategory" sender:self];
    }
    else if (rowIndex == searchOrganization){
        [self performSegueWithIdentifier:@"searchCharityOrganization" sender:self];
    }
    
    return nil;
}

#pragma makr - AsyncConnection Methods
- (void)finishConnectionWithData:(NSData *)data connection:(AsyncConnection *)connection{
    
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    NSLog(@"count %d",[jsonArray count]);
    self.items = [Item itemsWithArray:jsonArray];
    
    [self performSegueWithIdentifier:@"afterSearching" sender:self];
    
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    // modal view
    if ([segue.identifier isEqualToString:@"searchCategory"] || [segue.identifier isEqualToString:@"searchCharityOrganization" ]) {
        UIViewController *destinationController = segue.destinationViewController;
        if ([destinationController respondsToSelector:@selector(delegate)]) {
            [destinationController setValue:self forKey:@"delegate"];
        }
    }

    else{
        UIViewController *destination = segue.destinationViewController;
        
        // items for list item view
        if ([destination respondsToSelector:@selector(setItems:)]) {
            [destination setValue:self.items forKey:@"items"];
        }
    }
}

#pragma mark - Delegate method of SearchPickerDelegate
- (void)didFininishPickAnCategory:(Category *)category{
    [self dismissViewControllerAnimated:YES completion:nil];
    self.selectedCategory = category;
    self.categoryNameLabel.text = [category.name copy];
}

- (void)didUnSelectCategory{
    [self dismissViewControllerAnimated:YES completion:nil];
    self.selectedCategory = nil;
    self.categoryNameLabel.text = @"";
}

#pragma mark - Delegate methods of FYPSearchCharityViewDelegate
- (void)didFininishPickAnCharityOrganizations:(CharityOrganization *)charityOrganization{
    self.selectedCharityOrganization = charityOrganization;
    self.charityOrganizationLabel.text = charityOrganization.name;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didUnSelectCharityOrganization{
    self.selectedCharityOrganization = nil;
    self.charityOrganizationLabel.text = @"";
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
