//
//  FYPChildCategoryViewController.h
//  charity_donation_system
//
//  Created by Jason on 1/20/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FYPChildCategoryViewController : UITableViewController

@property (weak, nonatomic) NSArray* childCategories;

@end