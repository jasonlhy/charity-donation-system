//
//  FYPItemListViewController.m
//  charity_donation_system
//
//  Created by Jason on 1/20/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import "FYPItemListViewController.h"
#import "Item.h"

@interface FYPItemListViewController ()


@end

@implementation FYPItemListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"%@", _categoryId);
    
//    Item at category path

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // if this controller is called by child category controller
    if (self.categoryId !=nil) {
        NSDictionary* info = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Web Server Info"];
        NSString *ip = [info objectForKey:@"Domain ip"];
        NSString *action = [info objectForKey:@"Item at category path"];
        NSString *url = [[NSString alloc] initWithFormat:@"%@categories/%@/%@",ip,self.categoryId,action];
        
        NSLog(@"Category Item %@", url);
        
        NSData* jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        NSArray *items = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
        
        NSArray *itemArray = [Item itemsWithArray:items];
        
        self.items = itemArray;
    }


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    self.categoryId = nil;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    UIViewController *destination = segue.destinationViewController;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    NSDictionary *item = [_items objectAtIndex:indexPath.row];
    
    if ([destination respondsToSelector:@selector(setItem:)]) {
        [destination setValue:item forKey:@"item"];
    }
    
    

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ItemPreviewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    // Text Label
    NSUInteger row = indexPath.row;
    Item *item = [_items objectAtIndex:row];
    cell.textLabel.text = item.name;
    cell.imageView.image = [item itemIcon];
    return cell;
}

@end
