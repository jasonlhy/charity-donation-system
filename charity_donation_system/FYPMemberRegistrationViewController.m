//
//  FYPMemberRegistrationViewController.m
//  charity_donation_system
//
//  Created by Jason on 2/3/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import "FYPMemberRegistrationViewController.h"
#import "FYPViewController.h"

@interface FYPMemberRegistrationViewController ()

@end

@implementation FYPMemberRegistrationViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table Deleget Methods
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // return nil to prevent the table row being on focus
    // when the user tap the label
    return nil;
}


- (IBAction)finishedEditing:(UITextField *)sender {
    [sender resignFirstResponder];
}

- (IBAction)submitButtonClicked:(UIBarButtonItem *)sender {
    
    // validation
    BOOL *isValid = true;
    NSString *errorMessage = @"";
    NSString *password = self.passwordTextField.text;
    NSString *confirmPassword = self.confirmPasswordTextField.text;
    
    if (![password isEqualToString:confirmPassword]) {
        isValid = false;
        [errorMessage stringByAppendingString:@"The two passwords are not equal"];
    }
    
    if (!isValid) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warnning" message:@"The two passwords are not equal" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }
    
    // send to server
    // POST /members.json
    NSString *username = self.usernameTextField.text;
    NSString *email = self.emailTextField.text;
    NSString *mobileNo = self.mobileTextFiel.text;
    
    NSMutableDictionary *registerInfo =[[NSMutableDictionary alloc]init];
    [registerInfo setObject:username forKey:@"username" ];
    [registerInfo setObject:password forKey:@"password"];
    [registerInfo setObject:email forKey:@"email"];
    [registerInfo setObject:mobileNo forKey:@"mobileNo"];
    
    AsyncConnection *conc = [[AsyncConnection alloc]init];
    
    NSMutableDictionary *memberJson =[[NSMutableDictionary alloc]init];
    [memberJson setObject:registerInfo forKey:@"member"];
    
    NSLog(@"dic %@", memberJson);
   
    
//    // get Login info
//    NSDictionary* info = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Web Server Info"];
//    NSMutableString *fullLoginURL = [[info objectForKey:@"Domain ip"] mutableCopy];
//    NSString *loginPath = [info objectForKey:@"Login path"];
//    [fullLoginURL appendString:loginPath];
    
    [conc callAsyncConnectionAtUrl:@"http://localhost:3000/members.json" dictionary:memberJson method:@"POST" delegate:self];


}

#pragma mark - Asyonnection Delegate
- (void)finishConnectionWithData:(NSData *)data connection:(AsyncConnection *)connection{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thanks for registration" message:@"Welcome to charity donation system!" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    // return to login screen , and transit to menu
    NSUInteger numberOfController = [self.navigationController.viewControllers count];
    
    FYPViewController *loginController = [self.navigationController.viewControllers objectAtIndex:numberOfController-2];
    loginController.afterRegistration = YES;
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
