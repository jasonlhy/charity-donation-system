//
//  FYPItemDetailViewController.m
//  charity_donation_system
//
//  Created by Jason on 1/20/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import "FYPItemDetailViewController.h"
#import "MWPhoto.h"
#import "MWPhotoBrowser.h"
#import "Member.h"

@interface FYPItemDetailViewController ()

@end

@implementation FYPItemDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the
    

    
    // Set up the item detail
    self.nameField.text        = _item.name;
    self.nameHeadingLabel.text = _item.name;
    self.descriptionLabel.text = _item.description;
    self.categoryLabel.text    = [_item.category.name copy];
    self.charityOrganizationLabel.text = [_item.charityOrganization.name copy];
    self.endDateLabel.text     = _item.simpleEndDate;
    self.ratingLabel.text      = [[NSString alloc] initWithFormat:@"%@ %f",self.ratingLabel.text, _item.rating];
    self.sellerLabel.text = _item.seller.username;
    self.donationLabel.text = [[NSString alloc] initWithFormat:@"%.2f",_item.donationProportion];
    self.ratingLabel.text = [[NSString alloc] initWithFormat:@"%.2f",_item.rating];
    self.priceLabel.text = [[NSString alloc] initWithFormat:@"HKD$%.2f",  _item.price];
    
    NSLog(@"End date :%@",_item.endDate);
    
    // Image
    NSArray *imagesArray                = _item.itemImages;
    self.imageView.animationImages      = imagesArray;
    self.imageView.animationDuration    = 2*[imagesArray count];
    self.imageView.animationRepeatCount = 0;
    
    [self.imageView startAnimating];
    
    // Create array of `MWPhoto` objects
    self.photos = [NSMutableArray array];
    
    // Item Photo
    NSArray *itemPhotos = _item.itemPhotos;
    for (UIImage *image in imagesArray) {
        [self.photos addObject:[MWPhoto photoWithImage:image]];
    }
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.photos.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.photos.count)
        return [self.photos objectAtIndex:index];
    return nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // when photo row will be  selected
    if (indexPath.section==1 && indexPath.row == 0) {
        
        // Create & present browser
        MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
        // Set options
        browser.wantsFullScreenLayout = NO; // Decide if you want the photo browser full screen, i.e. whether the status bar is affected (defaults to YES)
        browser.displayActionButton = YES; // Show action button to save, copy or email photos (defaults to NO)
        [browser setInitialPageIndex:1]; // Example: allows second image to be presented first
        // Present
        [self.navigationController pushViewController:browser animated:YES];
    }
    

    
    
    // select name or description
    if ((indexPath.section == 0 && indexPath.row==0) || (indexPath.section == 1 && indexPath.row==1)) {
        [self performSegueWithIdentifier:@"showDetailText" sender:indexPath];
    }
    
    return nil;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    // prepare Segue for showing long text
    UIViewController *destinationController = segue.destinationViewController;
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:sender ];
    NSString *title = [cell.textLabel.text copy];
    NSString *longText = [cell.detailTextLabel.text copy];
    
    if ([destinationController respondsToSelector:@selector(titleOfText)]) {
        [destinationController setValue:title forKey:@"titleOfText"];
    }
    if ([destinationController respondsToSelector:@selector(longText)]) {
        [destinationController setValue:longText forKey:@"longText"];
    }
    
}

- (IBAction)buyButtonClicked:(id)sender {
    AsyncConnection *connection = [[AsyncConnection alloc] init];
    NSMutableDictionary *keyValue = [[NSMutableDictionary alloc] init];
    
    // Get the current Member
    Member *member = [Member member];
    [keyValue setObject:member.username forKey:@"username"];
    [keyValue setObject:member.password forKey:@"password"];
    [keyValue setObject:@(self.item.id) forKey:@"id"];
    [keyValue setObject:@(9999) forKey:@"input_price"];
    
    NSString *url = [[NSString alloc] initWithFormat:@"http://localhost:3000/items/%d/buy",self.item.id];
    [connection callAsyncConnectionAtUrl:url dictionary:keyValue method:@"PUT" delegate:self];
    
}

#pragma mark - AsyncConnectionDelegate
- (void)finishConnectionWithData:(NSData *)data connection:(AsyncConnection *)connection{
    self.buyButton.titleLabel.text = @"sold";
    self.buyButton.enabled = NO;
}
@end
