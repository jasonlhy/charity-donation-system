//
//  Category.m
//  charity_donation_system
//
//  Created by Jason on 1/19/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import "Category.h"

// json representation of all categories
//    Sample Json: ["{\"created_at\":\"2013-01-19T04:03:27+08:00\",\"id\":1,\"level\":1,\"name\":\"home\",\"previous_category_id\":null,\"updated_at\":\"2013-01-19T04:03:27+08:00\",\"sub_categories\":[{\"created_at\":\"2013-01-19T04:03:27+08:00\",\"id\":2,\"level\":null,\"name\":\"pen\",\"previous_category_id\":1,\"updated_at\":\"2013-01-19T04:03:27+08:00\"},{\"created_at\":\"2013-01-19T04:03:27+08:00\",\"id\":3,\"level\":null,\"name\":\"pencil\",\"previous_category_id\":1,\"updated_at\":\"2013-01-19T04:03:27+08:00\"}]}"]

@implementation Category

static NSArray* categories= nil;
static NSDictionary* categoriesDictionary = nil;

-(id)initWithData:(NSData *)data{
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    
    return [self initWithDictionary:dic];
}

-(id)initWithDictionary:(NSDictionary *)dictionary{
    
    
    NSLog(@"Category dictionary %@", dictionary);
    Category *category = [self init];
    
    // optional attribute
    if ([dictionary objectForKey:@"previous_category_id"] != [NSNull null]) {
        category.previousCategoryId = [[dictionary objectForKey:@"previous_category_id"] intValue];
    }
    
    // neccessary attribute
    category.id            = [[dictionary objectForKey:@"id"] intValue];
    category.level         = [[dictionary objectForKey:@"level"] intValue];
    category.name          = [dictionary objectForKey:@"name"];
    category.subCategories = [Category createCategoriesWithArray:[dictionary objectForKey:@"sub_categories"]];
    

    return category;
}

+(Category *)findCategoeryWithId:(NSUInteger)id{
    
    for (Category *category in categories){
        NSLog(@"category.id %d",category.id);
        if (category.id == id){
            return category;
        }
        else {
            for (Category *subCategory in category.subCategories){
                if (subCategory.id == id){
                    return subCategory;
                }
            }
        }
    }
    
    return nil;
}

// create singelton categireWithArray
+(NSArray *)categoriesWithArray:(NSArray *)jsonArray{
    
    if (categories == nil) {
        categories = [self createCategoriesWithArray:jsonArray];
    }
    
    return categories;
}

// private method to create categoriesWithArray
+(NSArray *)createCategoriesWithArray:(NSArray *)jsonArray{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (NSDictionary *categoryJSON in jsonArray) {
        Category *category = [[Category alloc] initWithDictionary:categoryJSON];
        [array addObject:category];
    }
    
    return array;
}

// singleton of all categoires
+(NSArray *)categories{
    return categories;
}

// singleton of all categories as key-value
+(NSDictionary *)categoriesDictionary{
    if (categoriesDictionary == nil) {

        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
        
        // all parent as key
        for (Category *parentCategory in categories) {
            
            // all childs as contents
            NSMutableArray *childArray = [[NSMutableArray alloc] init];
            for (Category *childCategory in parentCategory.subCategories) {
                [childArray addObject:childCategory];
            }
            
            [dictionary setObject:childArray forKey:parentCategory.name];
        }
        
        categoriesDictionary = dictionary;
    }
    
    NSLog(@"category dictionary %@", categoriesDictionary);
    return categoriesDictionary;
}

@end
