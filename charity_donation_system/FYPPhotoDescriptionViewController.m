//
//  FYPPhotoDescriptionViewController.m
//  charity_donation_system
//
//  Created by Jason on 4/7/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import "FYPPhotoDescriptionViewController.h"
#import "NSData+Base64.h"

#define kOFFSET_FOR_KEYBOARD 80.0


@interface FYPPhotoDescriptionViewController ()

@property (nonatomic)CGPoint originialCenter; // the original center when the view is loaded
@property (nonatomic, strong)NSMutableArray *textViews;

@end

@implementation FYPPhotoDescriptionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    self.originialCenter = self.view.center;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionView Data Source Methods
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [self.imageArray count];
}

#pragma mark - UICollectionViewDelegate Methods
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"photoCell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    // fill in the imageView with a photo
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
    imageView.image = [self.imageArray objectAtIndex:indexPath.item];
    
    
    if (self.textViews == nil) {
        self.textViews = [NSMutableArray new];
    }
    UITextView *textView = (UITextView *)[cell viewWithTag:textViewTag];
    [textView setDelegate:self];
    [self.textViews addObject:textView];

    
    return cell;
    
}

-(void) textViewDidBeginEditing:(UITextView *)textView {
    CGPoint pt;
    CGRect rc = [textView bounds];
    rc = [textView convertRect:rc toView:self.collectionView];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 50;
    [self.collectionView setContentOffset:pt animated:YES];
}

- (IBAction)postItem:(UIBarButtonItem *)sender {
    // the info data and photos is sent from the previous page
    // only convert the photo description for upload
    // photos
    NSMutableArray *encodedPhotoArray = [NSMutableArray new];
    for (int i =0; i<[self.imageArray count]; i++) {
        
        // encode the photo by base64
        NSMutableDictionary *itemPhoto= [NSMutableDictionary new ];
        NSData *imageData = UIImagePNGRepresentation([self.imageArray objectAtIndex:i]);
        NSString *imageDataEncodedString = [imageData base64EncodedString];
        
        // find the relative description
        NSString *description = [self contentInTextViewInCollectionViewAtRow:i];
        [itemPhoto setObject:description forKey:@"description"];
        [itemPhoto setObject:imageDataEncodedString forKey:@"file"];
        
        [encodedPhotoArray addObject:itemPhoto];
    }
    [self.infoJson setObject:encodedPhotoArray forKey:@"remote_itemPhotos_attribute"];
    
    // upload to server
    AsyncConnection *connection = [[AsyncConnection alloc] init];
    [connection callAsyncConnectionAtUrl:@"http://localhost:3000/items/create_with_photos" dictionary:self.infoJson method:@"POST" delegate:self];
    
    
}

#pragma mark - Delegeate of Asyc Connection
-(void)finishConnectionWithData:(NSData *)data connection:(AsyncConnection *)connection{
    
    // back to main menu and notice the uesr
    [self.navigationController popToViewController:self.mainMenuController animated:YES];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Notice" message:@"You have successfully post an item" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
}


#pragma mark - Customerized Method
-(NSString *)contentInTextViewInCollectionViewAtRow:(NSInteger)row{
//    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:row inSection:0];
//    UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
//    UITextView *textView = (UITextView *)[cell viewWithTag:textViewTag];
    // cannot use it, the collectionoview will reuse somecell,
    UITextView *textView = [self.textViews objectAtIndex:row];
   return textView.text;
}
@end
