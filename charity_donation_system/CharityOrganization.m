//
//  CharityOrganization.m
//  charity_donation_system
//
//  Created by Jason on 2/4/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import "CharityOrganization.h"

@implementation CharityOrganization

static NSArray* charities;

// init a Category
-(id)initWithData:(NSData *)data{
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    
    return [self initWithDictionary:dic];
}

-(id)initWithDictionary:(NSDictionary *)dictionary{
    CharityOrganization *charityOrganization = [self init];
    
    // neccessary attribute
    charityOrganization.id = [[dictionary objectForKey:@"id"] intValue];
    charityOrganization.name        = [dictionary objectForKey:@"name"];
    charityOrganization.description = [dictionary objectForKey:@"description"];
    charityOrganization.main_page   = [dictionary objectForKey:@"main_page"];

    
    return charityOrganization;
}

+(CharityOrganization *)findCharityOrganizationWithId:(NSUInteger)id{
    for (CharityOrganization *charity in charities){
        if (charity.id == id){
            return charity;
        }
    }
    
    return nil;
}

// create a array with categories from JSONArray
+(NSArray *)charityOrganizationsWithArray:(NSArray *)jsonArray{
    if (charities == nil) {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        
        for (NSDictionary *charityJSON in jsonArray) {
            CharityOrganization *charityOrganiztion = [[CharityOrganization alloc] initWithDictionary:charityJSON];
            [array addObject:charityOrganiztion];
        }
        
        charities = array;
        
    }
    return charities;
}

+(NSArray *)charityOrganizations{
    return charities;
}

@end
