//
//  FYPParentCategoryViewController.m
//  charity_donation_system
//
//  Created by Jason on 1/20/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import "FYPParentCategoryViewController.h"

@interface FYPParentCategoryViewController ()

@end

@implementation FYPParentCategoryViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    self.categories = [Category categories];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.categories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ParentCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    NSInteger row = indexPath.row;
    Category *category = [_categories objectAtIndex:row];
    cell.textLabel.text = category.name;
    
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    // find the index paht
    // get the correspodning parent category
    // set its child ctegory to destination
    UIViewController *destination = segue.destinationViewController;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    
    Category *category = [_categories objectAtIndex:indexPath.row];
    NSArray *childCategories = category.subCategories;
    
    NSLog(@"array %@",self.categories);

    
    if ([destination respondsToSelector:@selector(setChildCategories:)]) {
        [destination setValue:childCategories forKey:@"childCategories"];
    }
}


@end
