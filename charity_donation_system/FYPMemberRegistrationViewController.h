//
//  FYPMemberRegistrationViewController.h
//  charity_donation_system
//
//  Created by Jason on 2/3/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncConnection.h"

@interface FYPMemberRegistrationViewController : UITableViewController <AsyncConnectionDelegate>


@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileTextFiel;

- (IBAction)finishedEditing:(UITextField *)sender;
- (IBAction)submitButtonClicked:(UIBarButtonItem *)sender;


@end
