//
//  Item.m
//  charity_donation_system
//
//  Created by Jason on 1/21/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import "Item.h"
#import "ItemPhoto.h"

@implementation Item

@synthesize seller = _seller;

//
//{{"bid_id":null,"buyer_id":null,"category_id":8,"charity_organization_id":1,"created_at":"2013-02-04T02:11:15+08:00","description":"Size: 37\nNew:99%\nOnly wore once\nBought from yeswalker","donation_proportion":50,"end_date":"2013-02-11T02:11:14+08:00","id":1,"name":"Green Two-Tone Buckled Platform Heels","offer_id":null,"price":"150.0","rating":null,"seller_id":1,"start_date":"2013-02-04T02:11:14+08:00","step_size":null,"updated_at":"2013-02-04T02:11:15+08:00","itemPhotos":[{"created_at":"2013-02-04T02:11:15+08:00","description":"greate photo","file":{"url":"/uploads/item_photo/file/1/14.png"},"id":1,"item_id":1,"updated_at":"2013-02-04T02:11:15+08:00"},{"created_at":"2013-02-04T02:11:15+08:00","description":"greate photo","file":{"url":"/uploads/item_photo/file/2/11.png"},"id":2,"item_id":1,"updated_at":"2013-02-04T02:11:15+08:00"},{"created_at":"2013-02-04T02:11:15+08:00","description":"greate photo","file":{"url":"/uploads/item_photo/file/3/12.png"},"id":3,"item_id":1,"updated_at":"2013-02-04T02:11:15+08:00"},{"created_at":"2013-02-04T02:11:15+08:00","description":"greate photo","file":{"url":"/uploads/item_photo/file/4/13.png"},"id":4,"item_id":1,"updated_at":"2013-02-04T02:11:15+08:00"}]}

-(id)initWithData:(NSData *)data{
    return [self initWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil]];
}

-(id)initWithDictionary:(NSDictionary *)dictionary{
    NSLog(@"dictionary %@",dictionary);
    
    Item *item       = [self init];
  
    // optional attribute
    if ([dictionary objectForKey:@"price"] != [NSNull null]) {
        item.price   = [[dictionary objectForKey:@"price"]doubleValue];
    }
    if ([dictionary objectForKey:@"rating"] != [NSNull null]) {
        item.rating  = [[dictionary objectForKey:@"rating"] doubleValue];
    }
    if ([dictionary objectForKey:@"buyer_id"] != [NSNull null]) {
        item.buyerId = [[dictionary objectForKey:@"buyer_id"] intValue];
    }
    if ([dictionary objectForKey:@"offer_id"] != [NSNull null]) {
        item.offerId= [[dictionary objectForKey:@"offer_id"] intValue];
    }
    
 
    // neccessary attribute
    item.id          = [[dictionary objectForKey:@"id"] intValue];
    item.sellerId    = [[dictionary objectForKey:@"seller_id"] intValue];
    item.categoryId  = [[dictionary objectForKey:@"category_id"] intValue];
    item.donationProportion = [[dictionary objectForKey:@"donation_proportion"] doubleValue];
    item.charityOrganizationId = [[dictionary objectForKey:@"charity_organization_id"] intValue];
    
    item.startDate   = [self formatDateFromString:[dictionary objectForKey:@"start_date"]];
    item.endDate     = [self formatDateFromString:[dictionary objectForKey:@"end_date"]];
    item.name        = [dictionary objectForKey:@"name"];
    item.description = [dictionary objectForKey:@"description"];
    item.type        = [dictionary objectForKey:@"type"];
    item.status      = [dictionary objectForKey:@"status"];
    
    item.itemPhotos  = [ItemPhoto itemPhotosWithArray:[dictionary objectForKey:@"itemPhotos"]];

    
    return item;

}

+(NSArray *)itemsWithArray:(NSArray *)jsonArray{

    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSDictionary *dic in jsonArray) {
        Item *item = [[Item alloc] initWithDictionary:dic];
        [array addObject:item];
    }
    
    return array;
}

-(UIImage* )itemImageAtIndex:(NSUInteger)index{
    
    if ([self.itemPhotos count] == 0) {
        return nil;
    }
    ItemPhoto *itemPhoto = [self.itemPhotos objectAtIndex:index];
    return itemPhoto.image;
}

#pragma mark - customerize getter
-(NSArray *)itemImages{
    
    // lazy loading
    if (_itemImages == nil) {
        NSMutableArray *imagesArray = [[NSMutableArray alloc] init];
        NSArray *itemPhotos = self.itemPhotos;
        
        for (ItemPhoto *itemPhoto in itemPhotos) {
            [imagesArray addObject:itemPhoto.image];
        }
        
       _itemImages = imagesArray;
    }

    
    return _itemImages;

}

-(Category *)category{
     return [Category findCategoeryWithId:self.categoryId];
}

-(CharityOrganization *)charityOrganization{
    return [CharityOrganization findCharityOrganizationWithId:self.charityOrganizationId];
}

-(Member *)seller{
    if (_seller == nil) {
        NSString *url = [[NSString alloc] initWithFormat:@"http://localhost:3000/members/%d.json", self.sellerId];
        NSData* sellerData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        Member* theSeller = [Member initWithData:sellerData];
        _seller = theSeller;
    }
    
    return _seller;
}

#pragma mark - wrapper method
-(UIImage *)itemIcon{
    UIImage* image = [self itemImageAtIndex:0];
    return (image == nil)?nil:image;
}

#pragma mark - helper method
-(NSDate *)formatDateFromString:(NSString *)dateString{
    //2013-01-27T02:31:47+08:00
    
    NSDateFormatter *rfc3339DateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    [rfc3339DateFormatter setLocale:enUSPOSIXLocale];
    [rfc3339DateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    [rfc3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8]];
    
    // Convert the RFC 3339 date time string to an NSDate.
   // NSDate *date = [rfc3339DateFormatter dateFromString:@"2013-01-27T02:31:47+08:00"];
    NSDate *date = [rfc3339DateFormatter dateFromString:dateString];
    
    return date;
}

-(NSString *)simpleEndDate{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm"];
    return [dateFormat stringFromDate:self.endDate];
}

-(BOOL)isSold{
    
    return (self.offerId == 0)?NO:YES;
}

@end
