//
//  ItemPhoto.h
//  charity_donation_system
//
//  Created by Jason on 1/21/13.
//  Copyright (c) 2013 ive. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface ItemPhoto : NSObject

@property (nonatomic) NSUInteger id;
@property (strong, nonatomic) NSString* description;
@property (strong, nonatomic) NSURL* fileURL;
@property (strong, nonatomic) UIImage *image;

-(id)initWithData:(NSData *)data;
-(id)initWithDictionary:(NSDictionary *)dictionary;
+(NSArray *)itemPhotosWithArray:(NSArray *)jsonArray;

@end
