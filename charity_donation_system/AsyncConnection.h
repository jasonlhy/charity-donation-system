//
//  AsyncConnection.h
//  charity_donation_system
//
//  Created by Jason on 12/30/12.
//  Copyright (c) 2012 ive. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AsyncConnectionDelegate;

@interface AsyncConnection : NSObject <NSURLConnectionDataDelegate>
@property (weak, nonatomic) id <AsyncConnectionDelegate> delegate;

-(void)callAsyncConnectionAtUrl:(NSString *)url dictionary:(NSDictionary *)dictionary method:(NSString *)method delegate:(id)delegate;
@end

@protocol AsyncConnectionDelegate <NSObject>
- (void)finishConnectionWithData:(NSData *)data connection:(AsyncConnection *)connection;
@end
