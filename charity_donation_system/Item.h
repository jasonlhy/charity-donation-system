//
//  Item.h
//  charity_donation_system
//
//  Created by Jason on 1/21/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Category.h"
#import "CharityOrganization.h"
#import "Member.h"

/* An model class of Item */
@interface Item : NSObject

typedef enum {
    BiddingItem = 0,
    DirectSellItem = 1,
} ItemType;

#pragma - primitive data type
@property (nonatomic) NSUInteger id;
@property (nonatomic) double price;
@property (nonatomic) double rating;
@property (nonatomic) double donationProportion;
@property (nonatomic) NSUInteger buyerId;
@property (nonatomic) NSUInteger sellerId;
@property (nonatomic) NSUInteger categoryId;
@property (nonatomic) NSUInteger charityOrganizationId;
@property (nonatomic) NSUInteger offerId;

#pragma mark - pointer data type
@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSDate *endDate;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *description;
@property (strong, nonatomic) NSNumber *type;
@property (strong, nonatomic) NSString *status;

#pragma mark - assoication
@property (strong, nonatomic) NSArray *itemPhotos;
@property (weak, readonly) Category *category;
@property (weak, readonly) CharityOrganization* charityOrganization;
@property (weak, readonly) Member* seller;

#pragma mark - wrapper methods
@property (strong, nonatomic) NSArray *itemImages;

#pragma mark - init a item
-(id)initWithData:(NSData *)data;
-(id)initWithDictionary:(NSDictionary *)dictionary;

// wrapper method, get a item image at a index
-(UIImage* )itemImageAtIndex:(NSUInteger)index;
-(UIImage* )itemIcon;

-(NSString *)simpleEndDate;

#pragma mark - Class Method
+(NSArray *)itemsWithArray:(NSArray *)jsonArray;



@end
