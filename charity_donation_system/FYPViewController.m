//
//  FYPViewController.m
//  charity_donation_system
//
//  Created by Jason on 12/29/12.
//  Copyright (c) 2012 ive. All rights reserved.
//

#import "FYPViewController.h"
#import "Member.h"

@interface FYPViewController ()
@end

@implementation FYPViewController

- (void)viewDidLoad
{
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    

    
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    // pop back after register?
    if (self.afterRegistration) {
        [self performSegueWithIdentifier:@"login" sender:self];  
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

#pragma mark - IBAction
- (IBAction)textFieldFinishEditing:(id)sender {
    [sender resignFirstResponder];
}

- (IBAction)loginBtnPressed:(id)sender{
    
    // user input
    NSString *username = _usernameTextField.text;
    NSString *password = _pwdTextField.text;
    
    // create dictionary key-value pair for transformming into NSData
    NSMutableDictionary *loginKeyValue = [[NSMutableDictionary alloc]init];
    [loginKeyValue setObject:username forKey:@"username"];
    [loginKeyValue setObject:password forKey:@"password"];
        
    AsyncConnection *conc = [[AsyncConnection alloc]init];
    
    // get Login info
    NSDictionary* info = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Web Server Info"];
    NSMutableString *fullLoginURL = [[info objectForKey:@"Domain ip"] mutableCopy];
    NSString *loginPath = [info objectForKey:@"Login path"];
    [fullLoginURL appendString:loginPath];
    
    [conc callAsyncConnectionAtUrl:fullLoginURL dictionary:loginKeyValue method:@"POST" delegate:self];
}

- (IBAction)cancelBtnPressed:(id)sender {
    _usernameTextField.text = nil;
    _pwdTextField.text = nil;
}


#pragma mark - AsyncConnection Delegate Methods
- (void)finishConnectionWithData:(NSData *)data connection:(AsyncConnection *)connection{
    
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    if (jsonObject[@"username"] == nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ERROR!!" message:@"Login Failed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        // get back the section
        UITableViewHeaderFooterView *headerView= [self.tableView headerViewForSection:infoSectionIndex];
        headerView.textLabel.text = @"Try Again!!";
        
        
        [alert show];
    }
    else{
        [Member createMember:data];
        [self performSegueWithIdentifier:@"login" sender:self];
    }
    
}


#pragma mark - Table Deleget Methods
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // return nil to prevent the table row being on focus
    // when the user tap the label
    return nil;
}



@end
