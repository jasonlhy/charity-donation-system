//
//  FYPViewController.h
//  charity_donation_system
//
//  Created by Jason on 12/29/12.
//  Copyright (c) 2012 ive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncConnection.h"

#define infoSectionIndex 0

@interface FYPViewController : UITableViewController <UINavigationControllerDelegate,AsyncConnectionDelegate>

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *pwdTextField;

@property (nonatomic) BOOL afterRegistration;

- (IBAction)textFieldFinishEditing:(id)sender;

- (IBAction)loginBtnPressed:(id)sender;
- (IBAction)cancelBtnPressed:(id)sender;

@end