//
//  FYPParentCategoryViewController.h
//  charity_donation_system
//
//  Created by Jason on 1/20/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Category.h"

@interface FYPParentCategoryViewController : UITableViewController

@property (weak, nonatomic) NSArray* categories;

@end
