//
//  AsyncConnection.m
//  charity_donation_system
//
//  Created by Jason on 12/30/12.
//  Copyright (c) 2012 ive. All rights reserved.
//


#import "AsyncConnection.h"


@interface AsyncConnection ()
@property (strong, nonatomic) NSMutableData *downloadData;
@end

@implementation AsyncConnection

-(void)callAsyncConnectionAtUrl:(NSString *)url dictionary:(NSDictionary *)dictionary method:(NSString *)method delegate:(id)delegate{
    
    // set up delegate
    _delegate = delegate;
    
    // create the request with url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    // add header value and set http method
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:method];
    
    
    if (dictionary != nil) {
        // let the NSData object be the data of the request
        NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:nil];
        [request setHTTPBody:data];
    }

    
    // create connection
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (connection) {
        _downloadData = [[NSMutableData alloc] init];
    }
    else {
        NSLog(@"connection failed");
    }
}

// delegate method
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [_downloadData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [_delegate finishConnectionWithData:_downloadData connection:self];
    
}

-(void)dealloc{
    
}


@end