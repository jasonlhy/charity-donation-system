//
//  FYPItemDetailViewController.h
//  charity_donation_system
//
//  Created by Jason on 1/20/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Item.h"
#import "MWPhotoBrowser.h"
#import "AsyncConnection.h"


#define kPhoto 5

@class Item;

@interface FYPItemDetailViewController : UITableViewController <MWPhotoBrowserDelegate, AsyncConnectionDelegate>

# pragma mark - open API for other controllers
@property (weak, nonatomic) Item* item;

@property (weak, nonatomic) IBOutlet UILabel *nameHeadingLabel;

# pragma mark - UI
@property (weak, nonatomic) IBOutlet UILabel *nameField;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak,nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *charityOrganizationLabel;
@property (weak, nonatomic) IBOutlet UILabel *sellerLabel;

@property (weak, nonatomic) IBOutlet UILabel *donationLabel;

@property (weak, nonatomic) IBOutlet UILabel *endDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (strong, nonatomic) NSMutableArray *photos;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;

#pragma mark - Action
- (IBAction)buyButtonClicked:(id)sender;


@end
