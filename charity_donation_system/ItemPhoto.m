//
//  ItemPhoto.m
//  charity_donation_system
//
//  Created by Jason on 1/21/13.
//  Copyright (c) 2013 ive. All rights reserved.
//


#import "ItemPhoto.h"

@implementation ItemPhoto


-(id)initWithData:(NSData *)data{
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    return [self initWithDictionary:dic];
}

-(id)initWithDictionary:(NSDictionary *)dictionary{
    static NSString *ip = @"http://localhost:3000";
    

    
    ItemPhoto *itemPhoto = [self init];
    itemPhoto.id = [[dictionary objectForKey:@"id"] intValue];
    itemPhoto.description = dictionary[@"description"];
    NSString *string = [[NSString alloc] initWithFormat:@"%@%@",ip,[NSURL URLWithString:dictionary[@"file"][@"url"]]];
    itemPhoto.fileURL = [NSURL URLWithString:string];

    NSLog(@"ItemPHoto dic%@",dictionary);
    
    return itemPhoto;
}

+(NSArray *)itemPhotosWithArray:(NSArray *)jsonArray{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSDictionary *dic in jsonArray) {
        ItemPhoto *itemPhoto = [[ItemPhoto alloc] initWithDictionary:dic];
        [array addObject:itemPhoto];
    }
    
    return array;
}

#pragma - customize getter
-(UIImage *)image{
    
    // lazy loading
    if (_image == nil) {
        NSData* imageData = [NSData dataWithContentsOfURL:self.fileURL];
        UIImage *image = [[UIImage alloc] initWithData:imageData];
        _image= image;
    }
    
    return _image;
    
}

@end
