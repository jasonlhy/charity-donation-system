//
//  FYPLongTextViewController.m
//  charity_donation_system
//
//  Created by Jason on 2/3/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import "FYPLongTextViewController.h"

@interface FYPLongTextViewController ()

@end

@implementation FYPLongTextViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.textView.text = self.longText;
    self.navigationItem.title = self.titleOfText;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    [self.delegate didFinishEditingOnExit:self contentInTextView:self.textView.text];
}

@end
