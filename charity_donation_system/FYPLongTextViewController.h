//
//  FYPLongTextViewController.h
//  charity_donation_system
//
//  Created by Jason on 2/3/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FYPLongTextViewControllerDelegate;

@interface FYPLongTextViewController : UIViewController

@property (strong, nonatomic) NSString* titleOfText;
@property (strong, nonatomic) NSString* longText;

@property (weak, nonatomic) id <FYPLongTextViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@protocol FYPLongTextViewControllerDelegate <NSObject>

-(void)didFinishEditingOnExit:(FYPLongTextViewController *)controller contentInTextView:(NSString *)content;

@end
