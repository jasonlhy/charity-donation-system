//
//  CharityOrganization.h
//  charity_donation_system
//
//  Created by Jason on 2/4/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CharityOrganization : NSObject

@property (nonatomic) NSUInteger id;

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *description;
@property (strong, nonatomic) NSString *main_page;

// init a charity organization
-(id)initWithData:(NSData *)data;
-(id)initWithDictionary:(NSDictionary *)dictionary;


// find charity organization with the id providded
+(CharityOrganization *)findCharityOrganizationWithId:(NSUInteger)id;


// create a array with charity organizations from JSONArray
+(NSArray *)charityOrganizationsWithArray:(NSArray *)jsonArray;

+(NSArray *)charityOrganizations;

@end
