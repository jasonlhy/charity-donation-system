//
//  FYPMainMenuController.m
//  charity_donation_system
//
//  Created by Jason on 1/7/13.
//  Copyright (c) 2013 ive. All rights reserved.
//

#import "FYPMainMenuController.h"
#import "Category.h"
#import "CharityOrganization.h"

@interface FYPMainMenuController ()
@property (nonatomic) NSInteger selectedActionSheetIndex;
@end

@implementation FYPMainMenuController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{

    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
 
    // get Categories json(including the sub categories)
    NSDictionary* info = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Web Server Info"];
    NSString *ip = [info objectForKey:@"Domain ip"];
    
    NSString *categoriesPath = [info objectForKey:@"Categories path"];
    NSString * fullCategoriesURL = [ip stringByAppendingString:categoriesPath];
    NSData* jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:fullCategoriesURL]];
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
    [Category categoriesWithArray:jsonArray];
    
    // get Charity Organizatins json
    NSString *charitiesPath = [info objectForKey:@"CharityOrganizations path"];
    NSString *fullCharitesPath = [ip stringByAppendingString:charitiesPath];
    NSData* jsonData2 = [NSData dataWithContentsOfURL:[NSURL URLWithString:fullCharitesPath]];
    NSArray *jsonArray2 = [NSJSONSerialization JSONObjectWithData:jsonData2 options:NSJSONReadingMutableContainers error:nil];
    [CharityOrganization charityOrganizationsWithArray:jsonArray2];


 
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)postItemButtonClicked:(UIButton *)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Which type of item your want to post?" delegate:self cancelButtonTitle:@"No! Thanks" destructiveButtonTitle:nil otherButtonTitles:@"Bidding Item", @"Direct buy", nil];
    
    [actionSheet showInView:self.view];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    UIViewController *destinationController = segue.destinationViewController;
    
    if ([destinationController respondsToSelector:@selector(mainMenuController)]) {
        [destinationController setValue:self forKey:@"mainMenuController"];
    }

}

#pragma mark - UIActionSheet Delegate Methods
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    // do nothing when the user clicks thank you
    if (buttonIndex == directBuyActionSheetIndex) {
        self.selectedActionSheetIndex = buttonIndex;
        [self performSegueWithIdentifier:@"postDirectItem" sender:self];
        return;
    }
    
    NSLog(@"clicked index %d", buttonIndex);
    
}
@end
