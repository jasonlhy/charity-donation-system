//
//  Member.h
//  charity_donation_system
//
//  Created by Jason on 12/29/12.
//  Copyright (c) 2012 ive. All rights reserved.
//

#import <Foundation/Foundation.h>

// Singleton pattern of member
@interface Member : NSObject

@property (nonatomic) NSInteger id;
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *password;

+(Member *) createMember:(NSData *)data;
+(Member *) initWithData:(NSData *)data;

// singelton
+(Member *) member;
@end




